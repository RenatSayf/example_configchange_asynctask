package com.example.renat.example_config_changeasynctask;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity
{
    private TextView textView;
    private GetDataAsync asyncTask;

    private String KEY_TEXT_VIEW = "text_view";

    @Override
    protected void onSaveInstanceState(Bundle outState)
    {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TEXT_VIEW, textView.getText().toString());
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnAsyncTask = (Button) findViewById(R.id.btn_async_task);
        Button btnClean = (Button) findViewById(R.id.btn_clean);
        textView = (TextView) findViewById(R.id.textView);

        if (savedInstanceState != null)
        {
            textView.setText(savedInstanceState.getString(KEY_TEXT_VIEW));
        }


        btnAsyncTask.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                asyncTask = new GetDataAsync(MainActivity.this, new GetDataAsync.AsyncQueryListener()
                {
                    @Override
                    public void onTaskComplete(String result)
                    {
                        textView.setText(result);
                        asyncTask = null;
                    }
                });

                if (asyncTask.getStatus() != AsyncTask.Status.RUNNING)
                {
                    asyncTask.execute();
                }
            }
        });

        btnClean.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                textView.setText(null);
            }
        });

    }

}
