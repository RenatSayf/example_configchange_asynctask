package com.example.renat.example_config_changeasynctask;

import android.app.Activity;
import android.os.AsyncTask;

import java.lang.ref.WeakReference;
import java.util.concurrent.TimeUnit;

/**
 * Created by Renat on 09.03.2017.
 */

class GetDataAsync extends AsyncTask<Void, Void, String>
{
    private WeakReference<AsyncQueryListener> listener;
    private LockOrientation lockOrientation;
    private Activity activity;

    GetDataAsync(Activity activity, AsyncQueryListener listener)
    {
        this.activity = activity;
        lockOrientation = new LockOrientation(activity);
        setQueryListener(listener);
    }

    interface AsyncQueryListener
    {
        void onTaskComplete(String result);
    }

    private void setQueryListener(AsyncQueryListener listener)
    {
        this.listener = new WeakReference<>(listener);
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        lockOrientation.lock();
    }

    @Override
    protected String doInBackground(Void... voids)
    {
        try
        {
            TimeUnit.SECONDS.sleep(5);
        } catch (InterruptedException e)
        {
            e.printStackTrace();
        }

        return activity.getString(R.string.text_complete);
    }

    @Override
    protected void onPostExecute(String s)
    {
        super.onPostExecute(s);
        AsyncQueryListener listener = this.listener.get();
        if (listener != null)
        {
            listener.onTaskComplete(s);
        }
        lockOrientation.unLock();
    }

    @Override
    protected void onCancelled()
    {
        super.onCancelled();
        lockOrientation.unLock();
    }


}
